﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Vector3 pos1;
    public Vector3 pos2;
    public GameObject point1;
    public GameObject point2;
    public float platformSpeed = 1.0f;

    private bool moving;
    void Start()
    {
        pos1 = point1.transform.position;
        pos2 = point2.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(pos1, pos2, Mathf.PingPong(Time.time * platformSpeed, 1.0f));

    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.tag == "Player")
    //    {
    //        collision.collider.transform.SetParent(transform);
    //    }
    //}

    //private void OnCollisionExit2D(Collision2D collision)
    //{
    //    if (collision.gameObject.tag == "Player")
    //    {
    //        collision.collider.transform.SetParent(null);
    //    }
          
    //}
}
