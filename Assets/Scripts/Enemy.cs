﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public bool IsDead;
    Animator myAnimator;
    public int maxHealth = 5;
    int currentHealth;
    void Start()
    {
        myAnimator = GetComponent<Animator>();
        myAnimator.enabled = true;
        currentHealth = maxHealth;
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        Debug.Log("enemy is taking dmg");
        if (currentHealth <= 0)
        {
            Debug.Log("die() is being initiated now");
            Die();
        }
    }

    void Die()
    {
        Debug.Log("Bout to die");
        myAnimator.SetBool("IsDead", true);
        Debug.Log("he ded");
        ///Destroy(this.gameObject);
    }

    void Grave()
    {
        Destroy(this.gameObject);
    }
}
