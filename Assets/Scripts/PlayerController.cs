﻿using UnityEngine;
using System.Collections;
public class PlayerController : MonoBehaviour
{
    public float speed = 5f;
    public float jumpSpeed = 8f;
    private float movement = 0f;
    private Rigidbody2D rigidBody; //Gives player physics
    public Transform groundCheckPoint; //Location of where we check to see if player is on the ground
    public BoxCollider2D groundCheckBox;
    public float groundCheckRadius; //Buffer radius around the groundCheckRadius
    public LayerMask ground;//The ground layer mask
    public LayerMask Obstacle;// The obstacle layer mask
    public bool isTouchingGround;//Tracks if player is touching ground layer
    public bool isTouchingObstacle;//Tracks if player is touching obstacle layer

    public Transform pointA;//Top left corner of ground check rect
    public Transform pointB;//Bottom right corner of ground check rect

    private Collider2D coll; //Collider for ground objects

    private Animator KyleAnim; // KyleAnim is used to indicate which animation should be playing for the state machine.
    private enum State { idle, walking, jumping, falling, attacking } // State machine variables
    private State state = State.idle; //State machine variable is state and set to idle by default. This changes in the StateSwitch function

    public Vector3 respawnPosition; //where kyle respawns
    public LevelManager theLevelManager; //respawn logic

    public GameObject player;

    private GameObject currentOneWayPlatform;
    [SerializeField] private BoxCollider2D playerCollider;

    // Use this for initialization
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        KyleAnim = GetComponent<Animator>();

        respawnPosition = transform.position;

        theLevelManager = FindObjectOfType<LevelManager>();
    }

    // Update is called once per frame
    void Update()
    {
        MovementController();
        StateManager();
        KyleAnim.SetInteger("state", (int)state);
    }

    private void MovementController()
    {

        isTouchingGround = Physics2D.OverlapArea(pointA.position, pointB.position, ground);
        isTouchingObstacle = Physics2D.OverlapArea(pointA.position, pointB.position, Obstacle);


        movement = Input.GetAxis("Horizontal");
        if (movement > 0f)
        {
            rigidBody.velocity = new Vector2(movement * speed, rigidBody.velocity.y);
            transform.localScale = new Vector2(1, 1);
        }
        else if (movement < 0f)
        {
            rigidBody.velocity = new Vector2(movement * speed, rigidBody.velocity.y);
            transform.localScale = new Vector2(-1, 1);
        }
        else
        {
            rigidBody.velocity = new Vector2(0, rigidBody.velocity.y);
        }
        if (Input.GetButtonDown("Jump") && (isTouchingGround == true || isTouchingObstacle == true))
        {
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, jumpSpeed);

            state = State.jumping;
        }

    }



    private void StateManager()
    {
        if (state == State.jumping)
        {

            if (rigidBody.velocity.y < Mathf.Epsilon)
            {
                state = State.falling;
            }
        }
        else if (state == State.falling)
        {
            if (isTouchingGround || isTouchingObstacle)
            {
                state = State.idle;
            }
        }
        else if (Mathf.Abs(rigidBody.velocity.x) > Mathf.Epsilon)
        {
            state = State.walking;
        }
        else if (Input.GetKeyDown(KeyCode.M))
        {
            state = State.attacking;
        }
        else
        {
            state = State.idle;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Kill Plane")
        {
            //gameObject.SetActive(false); 

            //transform.position = respawnPosition;
            //This is being taken care of by the LevelManager now
            theLevelManager.Respawn();

        }
        if (other.tag == "Checkpoint")
        {
            respawnPosition = other.transform.position;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) //Make player child to moving platforms
    {
        //GameObject other = collision.gameObject;
        if (collision.gameObject.CompareTag("MGround") && isTouchingGround == false)
        {
            player.transform.parent = collision.transform;
            Debug.Log("Is child to platform");
        }

        if (state == State.jumping || state == State.falling)
        {
            player.transform.parent = null;
        }
    }

    private void OnCollisionExit2D(Collision2D collision) //Removes child relationship to moving platforms
    {
        if (collision.gameObject.CompareTag("MGround"))
        {
            player.transform.parent = null;
            Debug.Log("No longer child of platform");
        }


    }


}