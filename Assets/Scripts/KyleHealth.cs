﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KyleHealth : MonoBehaviour
{
    public int health;//Player health
    public int numOfHearts;//total number of hearts visible (maybe max health as well?)

    public Image[] hearts;//Array that holds heart sprites
    public Sprite fullHeart;//Full heart image
    public Sprite emptyHeart;//Empty heart image

    SpriteRenderer renderer;
    Color origColor;
    float flashTime = .15f;

    public LevelManager theLevelManager;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
        origColor = renderer.color;
        theLevelManager = FindObjectOfType<LevelManager>();
    }

    // Update is called once per frame
    void Update()
    {
      if (Input.GetKeyDown(KeyCode.Backspace)) // Damage debug command - TODO: Remove this before launch
        {
            TakeDamage(1);
        }
        HealthBar();
        DeathCheck();
    }
    void TakeDamage(int damage)
    {
        health -= damage;
        FlashStart();
        //healthBar.SetHealth(numOfHearts);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("colliding with enemy");
            TakeDamage(1);
            
        }

    }

    void FlashStart()
    {
        renderer.material.color = Color.red;
        Invoke("FlashStop", flashTime);
    }

    void FlashStop()
    {
        renderer.material.color = origColor;

    }

    void HealthBar()
    {
        if (health > numOfHearts) //Ensures number of hearts visible matches health
        {
            health = numOfHearts;
        }

        if ( health < 0)
        {
            health = 0;
        }

        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < health)
            {
                hearts[i].sprite = fullHeart;
            }
            else
            {
                hearts[i].sprite = emptyHeart;
            }

            if (i < numOfHearts)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }
    }

    void DeathCheck()
    {
        if (health <= 0)
        {
            theLevelManager.Respawn();
            Debug.Log("Player Died.");

            health = numOfHearts;
        }
    }
}
