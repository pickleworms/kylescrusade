﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CounterBehavior : MonoBehaviour
{
    private static int coinTotal;
    private Text coinText;
    // Start is called before the first frame update
    void Start()
    {
        coinText = GetComponent<Text>();
        coinTotal = 0;
    }

    // Update is called once per frame
    void Update()
    {
        /*if(Input.GetKeyDown(KeyCode.P)) // This if statement is for testing reasons only.
        {                               // Pressing the P key adds to coinTotal.
            coinTotal++;
        }*/
        coinText.text = " " + coinTotal; // Empty space before the coinTotal is displayed is for spacing reasons.
    }
    
      public static void AddCoin()
      {
           coinTotal += 1;
      }
     
}
