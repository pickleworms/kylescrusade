﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBehavior : MonoBehaviour
{
    void onCollisionEnter2D(Collision2D coinCol)
    {
        if(coinCol.gameObject.tag == "Player")
        {
            Debug.Log("Kyle picked up a coin!");
            CounterBehavior.AddCoin(); // Calls to CounterBehavior.cs for AddCoin, adding one coin to the counter.
            Destroy(gameObject); // Destroys the in-world coin
        }
    }
}
