﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityScript : MonoBehaviour
{
    public float mGravity;
    public float mJumpForce;
    public float mCurrentVelocityDueToGravity;
    public bool mIsGrounded;
    public float mMovementSpeed;
    public Transform playerTransform;
    private bool mDidJumpThisFixedUpdate;

    void Start()
    {
        mGravity = -40;
        mJumpForce = 15;
        mMovementSpeed = 5;

    }

    void Update()
    {
        //jump
        if (Input.GetKeyDown(KeyCode.Space))
        {
            mDidJumpThisFixedUpdate = true;
        }
        // move player right
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += new Vector3(mMovementSpeed, 0, 0) * Time.deltaTime;
        }
        // move player left
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position += new Vector3(-mMovementSpeed, 0, 0) * Time.deltaTime;
        }
        //MKK 3_13_2021 Fix:  Need to set mCurrentVelocityDueToGravity to 0 here, not in OnTriggerExit
        if (mIsGrounded == true)
        {
            mCurrentVelocityDueToGravity = 0;
        }
        mCurrentVelocityDueToGravity += mGravity * Time.deltaTime;
        if (mIsGrounded == false)
        {
            transform.position += new Vector3(0, mCurrentVelocityDueToGravity, 0) * Time.deltaTime;
        }
    }
    public Vector3 mPreviousPlayerPosition;
    public Vector3 mCurrentPlayerPosition;
    private void FixedUpdate()
    {
        mPreviousPlayerPosition = mCurrentPlayerPosition;
        mCurrentPlayerPosition = playerTransform.position;
        if (mDidJumpThisFixedUpdate == true)
        {
            mIsGrounded = false;
            mCurrentVelocityDueToGravity = 0;
            mCurrentVelocityDueToGravity += mJumpForce;
            mDidJumpThisFixedUpdate = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        //alternative way to get the transform component instead of doing 
        //GetComponent<Transform>().position = new Vector3(transform.position.x, other.transform.position.y, transform.position.z);
        //Debug.LogError(mPreviousPlayerPosition.y.ToString("F4") + "/" + mCurrentPlayerPosition.y.ToString("F4") + "/" + other.transform.position.y.ToString("F4"));
        //MKK 3_13_2021 Fix:  this check of mPreviousPlayerPosition was not working before because I was checking the .z position (forward and backward), and not the .y position (up and down)
        if (mPreviousPlayerPosition.y > other.transform.position.y)
        {
            playerTransform.position = new Vector3(transform.position.x, other.transform.position.y, transform.position.z);
            mIsGrounded = true;
        }
    }
    private void OnTriggerStay(Collider other)
    {
    }
    private void OnTriggerExit(Collider other)
    {
        mIsGrounded = false;
    }
}