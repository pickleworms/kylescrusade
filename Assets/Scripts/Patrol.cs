﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    public float speed;
    public float chaseSpeed;
    public float distance;
    public bool movingRight = true;
    public bool movingLeft;
    public bool chaseActivated;

    public Transform groundDetection;
    public GameObject player;
    public float detectionDistance;
    public float heightThreshold;

    //These are from the enemyCollDetect script
    public Transform enemyCollDetec;
    public float detectRange = 0.5f;
    public LayerMask Obstacles;
    public bool enemyIsDetect;
    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        player = GameObject.Find("Kyle");
        detectionDistance = 10;
        heightThreshold = 0.1f;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        Patrolling();
        collDetect();
        CheckPlayerDistance();

    }
    //enemy needs to check to see how close kyle is
    //enemy needs to turn to face kyle when it knows kyle is within distance
    //enemy needs to chase kyle after kyle is detected by the enemy
    //enemy should stop its chase when kyle is X distance out of range

    private void CheckPlayerDistance()
    {
        if (Vector3.Distance(player.transform.position, transform.position) <= detectionDistance
            && Mathf.Abs(player.transform.position.y - transform.position.y) <= heightThreshold)
        {
            chaseActivated = true;
            //Debug.Log("Player in distance" + (player.transform.position - transform.position));
        }
    }

    private void ChasePlayer()
    {
        transform.Translate(Vector2.right * chaseSpeed * Time.deltaTime);
    }

    private void Patrolling()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);
        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, distance);

        if (groundInfo.collider == false)
        {
            if (movingRight == true)
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                movingRight = false;

            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingRight = true;
            }
        }
    }

    void collDetect() //Detects collison for enemy
    {
        enemyIsDetect = Physics2D.OverlapCircle(enemyCollDetec.position, detectRange, Obstacles); //Enemy is detecting when it intersects with things with Layer Obstacle within a range (detectrange)
        if (enemyIsDetect == true && movingRight == true)
        {

            transform.eulerAngles = new Vector3(0, -180, 0); //enemy turns around -180 degrees (left)
            movingRight = false;
            movingLeft = true;
        }
        else if (enemyIsDetect == true && movingRight == false) // enemy turns back to the right
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
            movingRight = true;
            movingLeft = false; 
        }
    }

    void OnDrawGizmosSelected()
    {
        if (enemyCollDetec == null)
            return;
        Gizmos.DrawWireSphere(enemyCollDetec.position, detectRange);
    }

}