﻿using UnityEngine;

public class WorldCamera : MonoBehaviour
{
    public Transform target;
    public float smoothSpeed = 0.125f; //higher number the faster the camera moves to the player
    public Vector3 offset;
    public Camera camera1;
    //max camera zoom amount
    //min camera zoom amount
    //camera zoom speed

    public void Start()
    {
        camera1 = GetComponent<Camera>();
    }

    public void Update()
    {
        CameraZoom();  
    }

    public void CameraZoom()
    {

        //function to zoom camera out when in volume
        if (Input.GetKey(KeyCode.UpArrow))
        {
            camera1.orthographicSize += 1 * Time.deltaTime;
        }

        //if (camera1.orthographicSize >= 8)
        //{
        //    camera1.orthographicSize = 8;
        //}
    }

    public void OnTriggerEnter(Collider other)
    {
        Debug.LogError("I hit a trigger");
    }

    public void OnTriggerExit(Collider other)
    {
        Debug.LogError("I left a trigger");
    }

    private void FixedUpdate()
    {
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed*Time.fixedDeltaTime);
        transform.position = smoothedPosition;
    }
}