﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour
{
    private Rigidbody2D KyleRB; //KyleRB is Kyle's Rigid Body. This gives him physics.
    private Animator KyleAnim; // KyleAnim is used to indicate which animation should be playing for the state machine.
    private enum State { idle, walking, jumping, falling, attacking } // State machine variables
    private State state = State.idle; //State machine variable is state and set to idle by default. This changes in the StateSwitch function
    private Collider2D coll; //Collider for ground objects
    //public int maxHealth = 100; //Sets players max health
    //public int currentHealth; //Tracks player's current health

    //public HealthBar healthBar;

    [SerializeField] private LayerMask ground; //LayerMask
    [SerializeField] private float speed = 5f; //How fast Kyle moves left and right
    [SerializeField] private float jumpSpeed = 15f; //How fast Kyle jumps

    public Vector3 respawnPosition; //where kyle respawns
    public LevelManager theLevelManager; //respawn logic

    void Start() //Start is called before the first frame update
    {
        //currentHealth = maxHealth; //Sets players health to the max amount of health
        //healthBar.SetMaxHealth(maxHealth); //Updates the healthbar UI

        KyleRB = GetComponent<Rigidbody2D>();
        KyleAnim = GetComponent<Animator>();
        coll = GetComponent<Collider2D>();
        //currentHealth = maxHealth;

        respawnPosition = transform.position;

        theLevelManager = FindObjectOfType<LevelManager>();
    }

    void Update() //Update is called once per frame
    {
        MovementManager();
        StateSwitcher();
        KyleAnim.SetInteger("state", (int)state);

        //if (Input.GetKeyDown(KeyCode.Z))
        //{
        //    TakeDamage(1);
        //}

    }


    private void MovementManager()
    {
        float hDirection = Input.GetAxis("Horizontal");

        //if (hDirection < 0)
        //{
        //    Debug.LogError("inside Hdirection < 0");
        //    KyleRB.velocity = new Vector2(-speed, KyleRB.velocity.y);
        //    transform.localScale = new Vector2(-1, 1);

        //}
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            KyleRB.velocity = new Vector2(-speed, KyleRB.velocity.y);
            transform.localScale = new Vector2(-1, 1);

        }
        //else if (hDirection > 0)
        //{
        //    Debug.LogError("inside Hdirection > 0");
        //    KyleRB.velocity = new Vector2(speed, KyleRB.velocity.y);
        //    transform.localScale = new Vector2(1, 1);

        //}
        if (Input.GetKey(KeyCode.RightArrow))
        {
            KyleRB.velocity = new Vector2(speed, KyleRB.velocity.y);
            transform.localScale = new Vector2(1, 1);
        }

        if (Input.GetButtonDown("Jump") && coll.IsTouchingLayers(ground))
        {
            KyleRB.velocity = new Vector2(KyleRB.velocity.x, jumpSpeed);
            state = State.jumping;
        }
    }
    private void StateSwitcher()
    {
        if (state == State.jumping)
        {
            if (KyleRB.velocity.y < Mathf.Epsilon)
            {
                state = State.falling;
            }
        }
        else if (state == State.falling)
        {
            if (coll.IsTouchingLayers(ground))
            {
                state = State.idle;
            }
        }
        else if (Mathf.Abs(KyleRB.velocity.x) > Mathf.Epsilon)
        {
            state = State.walking;
        }
        else if (Input.GetKeyDown(KeyCode.M))
        {
            state = State.attacking;
        }
        else
        {
            state = State.idle;
        }

    }
    void OnCollisionEnter2D(Collision2D other)
    {

        if (other.gameObject.CompareTag("MGround"))
        {
            this.transform.parent = other.transform;
        }

        if (other.gameObject.CompareTag("cameraVolume"))
        {
            Debug.LogError("I hit the cameraVolume");
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {

        if (other.gameObject.CompareTag("MGround"))
        {
            this.transform.parent = null;
        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Kill Plane")
        {
            //gameObject.SetActive(false); 

            //transform.position = respawnPosition;
            //This is being taken care of by the LevelManager now
            theLevelManager.Respawn();

        }
        if (other.tag == "Checkpoint")
        {
            respawnPosition = other.transform.position;
        }
    }
}


//    void TakeDamage(int damage)
//    {
//        currentHealth -= damage;

//        healthBar.SetHealth(currentHealth);
//    }
//}